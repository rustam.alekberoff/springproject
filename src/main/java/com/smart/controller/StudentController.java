package com.smart.controller;

import com.smart.dto.Student;
import com.smart.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RestController
@RequestMapping(value="/student")
@RequiredArgsConstructor
public class StudentController {


    private final StudentService studentService;
    private long id;

    @GetMapping(value="/id/{id}")
    public Student getStudentByID(@PathVariable long id){
        this.id = id;
        return studentService.getStudentbyId(id);
    }
    @GetMapping(value="/")
    public Student getStudentByID2(@PathParam(value="id") long id){
        return studentService.getStudentbyId(id);
    }


}
