package com.smart.service;

import com.smart.dto.Student;

public interface StudentService {

    Student getStudentbyId(long id);
    Student createStudent(Student obj);
    Student saveStudent(long id,Student obj);
    void delete(Long id);
}
