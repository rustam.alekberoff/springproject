package com.smart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


//@ComponentScan(basePackages={"com.smart.controller"})
@SpringBootApplication
public class Ms11Application {

	public static void main(String[] args) {
		SpringApplication.run(Ms11Application.class, args);
	}

}
